# dotfiles
This repository is dedicated to my system configuration, backups, etc.

![Screenshot](misc/screen.jpg)

## Wallpapers

![wallpapers](misc/wallpapers/bg-pc.jpg)
