export ZSH=$HOME/.config/oh-my-zsh
export EDITOR='nvim'
export VISUAL='nvim'
export BROWSER='google-chrome-stable'
#export TERM='alacritty'
#export TERMINAL='alacritty'
export DOTFILES=$HOME/.dotfiles
export ROFI_THEME=$HOME/.config/rofi/launchers/type-2/style-2.rasi
export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
export PATH="$PATH:$GEM_HOME/bin"
export PATH="/opt/homebrew/opt/ruby/bin:$PATH"

export GPG_TTY=$(tty)

# OpenJDK
export JAVA_HOME=/usr/lib/jvm/java-17-openjdk
export PATH_TO_FX=$JAVA_HOME/lib
export LD_LIBRARY_PATH=/home/mykhailobarsukov/Documents/University/Java\ libs/javafx-sdk-17.0.7/lib:$LD_LIBRARY_PATH

# Android Studio
export ANDROID_HOME=$HOME/Android/Sdk
export ANDROID_ROOT=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools

export FZF_DEFAULT_COMMAND='ag -g "" --hidden'
export FZF_DEFAULT_OPTS='--height 50% --border'
export NNN_BMS='f:/srv/http/ficus-wordpress;d:~/.dotfiles;p:~/Pictures;'
export NNN_COLORS='3219'
export NNN_DE_FILE_MANAGER='nautilus'
export NNN_FALLBACK_OPENER='gio open'
export NNN_FALLBACK_OPENER='gvfs-open'
export NNN_FALLBACK_OPENER='xdg-open'
export NNN_FIFO='/tmp/nnn.fifo'
export NNN_PLUG='d:dragdrop;p:preview-tui;t:imgview;x:xdgdefault;f:fzcd;'
export NNN_TRASH=1

ZSH_THEME='main'
DISABLE_UPDATE_PROMPT='true'
DISABLE_AUTO_UPDATE=true
plugins=(git)

source $ZSH/oh-my-zsh.sh
# source $DOTFILES/.config/zsh/.zshrc-xorg
# source $DOTFILES/.config/zsh/.zshrc-wayland

# NVM
NVM_DIR="$HOME/.nvm"
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion


n () {
  # Block nesting of nnn in subshells
  if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
    echo "nnn is already running"
    return
  fi

  export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

  nnn -deR "$@"

  if [ -f "$NNN_TMPFILE" ]; then
    . "$NNN_TMPFILE"
    rm -f "$NNN_TMPFILE" > /dev/null
  fi
}

nnn_cd() {
  if ! [ -z "$NNN_PIPE" ]; then
    printf "%s\0" "0c${PWD}" > "${NNN_PIPE}" !&
  fi
}

trap nnn_cd EXIT

function f() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

remove_packets() {
  for i in $(pacman -Qdtq); do
    pacman -Qi $i | ag Name
    pacman -Qi $i | ag Description
    sudo pacman -R $i;
  done
}

gcmp () {
  g++ $* && ./a.out
}

lrgst () {
  if [ $1 ]; then
    sudo du -hsx $1 | sort -rh | head -10
  else
    sudo du -hsx * | sort -rh | head -10
  fi
}

stopwatch () {
  start=$(date +%s)
  while true; do
    time="$(( $(date +%s) - $start))"
    printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
    sleep 0.1
  done
}

# Count files by extensions in a specified directory
count_files_by_ext () {
  find $1 -maxdepth 1 -type f | grep -o ".[^.]\+$" | sort | uniq -c
}

# Recursively count files by extensions in a specified directory
rec_count_files_by_ext () {
  find $1 -type f | grep -o ".[^.]\+$" | sort | uniq -c
}

fzcd() {
    local root_dir="$1" # Capture the first argument as the root directory
    if [[ -z "$root_dir" ]]; then
        echo "Usage: fzcd <root-directory>"
        return 1 # Exit the function if no directory is provided
    fi
    cd "$(find "$root_dir" -type d | fzf)" || return
}

# size_check function
# Description: Calculates and sorts the disk usage of specified directories and prints it in reverse order.
# Usage: size_check [-o output_file] directory1 [directory2 ...]
# Options:
#   -o output_file: Optional. Specifies the file where the sorted disk usage results will be saved.
#   directory: One or more directories to analyze. Must be valid directory paths.
function size_check() {
    local OUTPUT_FILE=""  # Initialize output file variable as empty

    # Process options
    while [[ "$1" == -* ]]; do
        case "$1" in
            -o) OUTPUT_FILE="$2"; shift 2;;  # Assign and shift past the output file option
            --) shift; break;;                # Allow end of options
            -*) echo "Unknown option: $1" >&2; return 1;;  # Handle unknown options
        esac
    done

    # Check if any directories are provided
    if [ $# -eq 0 ]; then
        echo "Error: No directories provided. Specify one or more directories to analyze."
        return 1
    fi

    # Run du and sort the results by human-readable sizes in reverse order
    if [ -z "$OUTPUT_FILE" ]; then
        # If no output file is specified, print to the screen
        {
            for dir in "$@"; do
                if [ -d "$dir" ]; then
                    du -sh "$dir"/*
                else
                    echo "Error: $dir is not a directory" >&2
                fi
            done
        } | sort -hr
    else
        # If an output file is specified, save to file
        {
            for dir in "$@"; do
                if [ -d "$dir" ]; then
                    du -sh "$dir"/*
                else
                    echo "Error: $dir is not a directory" >&2
                fi
            done
        } | sort -hr >> "$OUTPUT_FILE"
        echo "Size check completed. Results are in $OUTPUT_FILE"
    fi
}


## Aliases
alias Agh='sudo ag --hidden -g'
alias N='sudo nnn -deHR'
alias Nv='sudo nvim'
alias agh='ag --hidden -g'
alias ascii='less ~/.local/share/etc/ascii'
alias feh='feh -d --scale-down'
alias l='eza --color=always -l --git --no-filesize --icons=always --no-time --no-user --no-permissions -A'
alias ls='eza --color=always -l --git --no-filesize --icons=always --no-time --no-user --no-permissions'
alias nf='neofetch'
alias nv='nvim'
alias p='python3'
alias cfbe='count_files_by_ext'
alias rcfbe='rec_count_files_by_ext'

# Bluetooth & Network
alias conadd='nmcli device connect $(ls /sys/class/net | grep -o "wl.*")'
alias inet='ping archlinux.org'
alias myip='curl icanhazip.com'
alias btc='bluetoothctl'
alias btrestart='sudo systemctl restart bluetooth'

## Screensavers
alias matrix="cmatrix -a -b -C cyan -u 3"
alias rain='sh ~/.local/share/etc/rain.sh'
alias wtime='tty-clock -cbnC 4'

## Pacman
alias Qi='pacman -Qi'                         # Package info
alias Qk='pacman -QK'                         # Number of package files
alias Ql='pacman -Ql'                         # List of package files
alias Qs='pacman -Qs'                         # Short description
alias Qu='pacman -Qu'                         # List upgradable
alias R!='sudo pacman -Rcns'                  # Remove a package with dependencies
alias R='sudo pacman -R'                      # Remove a package
alias S='sudo pacman -S'                      # Install a package
alias Ss='pacman -Ss'                         # Search package
alias Sy='sudo pacman -Sy'                    # Update db
alias Syu='yay -Syu && sudo pacman -Syu'      # Full upgrade
alias Syuu='yay -Syuu && sudo pacman -Syuu'   # Event fuller upgrade
# Remove unneccessary packages
alias Rdt='remove_packets'


### MANAGED BY RANCHER DESKTOP START (DO NOT EDIT)
export PATH="/Users/user/.rd/bin:$PATH"
### MANAGED BY RANCHER DESKTOP END (DO NOT EDIT)
