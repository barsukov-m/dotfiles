#!/bin/bash

# Get the current battery percentage
BATTERY_PERCENT=$(acpi -b | grep -P -o '[0-9]+(?=%)')

# If battery percentage is less than or equal to 40, send a notification
if [ "$BATTERY_PERCENT" -le 40 ]; then
    notify-send "Consider charging" "Battery is at $BATTERY_PERCENT%"
fi
