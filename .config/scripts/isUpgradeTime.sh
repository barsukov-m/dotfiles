PKG_COUNT=$(pacman -Qu | wc -l)

if [[ "$PKG_COUNT" -gt 20 ]]; then
  notify-send "📥 Upgrade time!" "You have $PKG_COUNT outdated packages.";
fi
