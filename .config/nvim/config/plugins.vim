" Undotree
nnoremap <silent> tu :UndotreeToggle<cr>
let g:undotree_WindowLayout = 3
let g:undotree_SetFocusWhenToggle = 1

if has("persistent_undo")
	let target_path = expand('~/.undodir')

	" create the directory and any parent directories
	" if the location does not exist.
	if !isdirectory(target_path)
		call mkdir(target_path, "p", 0700)
	endif

	let &undodir=target_path
	set udf
endif


" Panels & barbar (tabs)
inoremap <silent> <c-h> <c-w>
nnoremap <silent> <c-h> <esc><c-w>h
nnoremap <silent> <c-j> <esc><c-w>j
nnoremap <silent> <c-k> <esc><c-w>k
nnoremap <silent> <c-l> <esc><c-w>l
nnoremap <silent> <m-s-h> :BufferMovePrevious<cr>
nnoremap <silent> <m-s-l> :BufferMoveNext<cr>
nnoremap <silent> <m-h> :BufferPrevious<cr>
nnoremap <silent> <m-l> :BufferNext<cr>
nnoremap <silent> <c-w><cr> :BufferClose<cr>
" let bufferline = get(g:, 'bufferline', {})
" let bufferline.animation = v:false
" let bufferline.auto_hide = v:true
" let bufferline.tabpages = v:false


" NNN
" nnoremap <silent> ; :NnnPicker %:p:h<cr>
" nnoremap <silent> \| :NnnExplorer %:p:h<cr>
" let g:nnn#layout = { 'window': { 'width': 0.6, 'height': 0.8, 'highlight': 'Comment' } }
" let g:nnn#action = {
" 	\ '<c-t>': 'tab split',
" 	\ '<c-x>': 'split',
" 	\ '<c-s>': 'vsplit' }
" let g:nnn#session = 'local'
" let g:nnn#command = 'nnn -e'


" Yazi
nnoremap <silent> ; :Yazi<cr>


" FZF
nnoremap <c-f> :Files ~/<cr>
" nnoremap <c-f> :FZF ~/<cr>
" let g:fzf_action = {
"   \ 'ctrl-t': 'tab split',
"   \ 'ctrl-x': 'split',
"   \ 'ctrl-s': 'vsplit'
"   \ }


" CoC
set updatetime=300
set shortmess+=c

if has("nvim-0.5.0") || has("patch-8.1.1564")
	set signcolumn=number
else
	set signcolumn=yes
endif

" show suggestions
" function! s:check_back_space() abort
" 	let col = col('.') - 1
" 	return !col || getline('.')[col - 1]  =~# '\s'
" endfunction


" inoremap <silent><expr> <tab>
" 	\ pumvisible() ? "\<c-n>" :
" 	\ <SID>check_back_space() ? "\<tab>" :
" 	\ coc#refresh()

inoremap <expr><s-tab> pumvisible() ? "\<c-p>" : "\<c-h>"
inoremap <silent><expr> <c-space> coc#refresh()

" enter to expand
inoremap <expr> <cr> pumvisible() ? "\<c-y>" : "\<c-g>u\<cr>"


" Prettier
nnoremap <silent> fp :CocCommand prettier.formatFile<cr>


" Snippets
let g:UltiSnipsExpandTrigger="<Nop>"
let g:UltiSnipsSnippetsDir=expand('~/.vim/UltiSnips')
let g:UltiSnipsSnippetDirectories=['UltiSnips']


" Startify
let g:startify_files_number = 5

" bookmarks
let g:startify_bookmarks = [
  \ {'d': '~/.dotfiles'},
  \ {'n': '~/.config/nvim/init.vim'},
  \ ]

" list of links
let g:startify_lists = [
  \ { 'type': 'dir',       'header': ['  Recent files'] },
  \ { 'type': 'sessions',  'header': ['  Saved sessions'] },
  \ { 'type': 'bookmarks',  'header': ['  Bookmarks'] },
  \ ]

" custom header
let g:startify_custom_header = [
  \ "    ┌┐┌┌─┐┌─┐┬  ┬┬┌┬┐",
  \ "    │││├┤ │ │└┐┌┘││││",
  \ "    ┘└┘└─┘└─┘ └┘ ┴┴ ┴",
  \ ]

hi StartifyHeader  guifg=#fabc2e
" hi StartifyBracket guifg=
" hi StartifyFile    guifg=
" hi StartifyFooter  guifg=
hi StartifyNumber  guifg=#fabc2e
" hi StartifyPath    guifg=
" hi StartifySlash   guifg=
" hi StartifySpecial guifg=


lua << EVILINE

-- Eviline config for lualine
-- Author: shadmansaleh
-- Credit: glepnir
local lualine = require 'lualine'

-- Color table for highlights
-- stylua: ignore
local colors = {
  bg       = '#1d2021',
  fg       = '#ebdbb2',
  yellow   = '#fabc2e',
  cyan     = '#8ec07b',
  darkblue = '#928373',
  green    = '#b8ba25',
  orange   = '#d79920',
  violet   = '#b16185',
  magenta  = '#d3859a',
  blue     = '#83a597',
  red      = '#fb4833',
}

local conditions = {
  buffer_not_empty = function()
    return vim.fn.empty(vim.fn.expand '%:t') ~= 1
  end,
  hide_in_width = function()
    return vim.fn.winwidth(0) > 80
  end,
  check_git_workspace = function()
    local filepath = vim.fn.expand '%:p:h'
    local gitdir = vim.fn.finddir('.git', filepath .. ';')
    return gitdir and #gitdir > 0 and #gitdir < #filepath
  end,
}

-- Config
local config = {
  options = {
    -- Disable sections and component separators
    component_separators = '',
    section_separators = '',
    theme = {
      -- We are going to use lualine_c an lualine_x as left and
      -- right section. Both are highlighted by c theme .  So we
      -- are just setting default looks o statusline
      normal = { c = { fg = colors.fg, bg = colors.bg } },
      inactive = { c = { fg = colors.fg, bg = colors.bg } },
    },
  },
  sections = {
    -- these are to remove the defaults
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    -- These will be filled later
    lualine_c = {},
    lualine_x = {},
  },
  inactive_sections = {
    -- these are to remove the defaults
    lualine_a = {},
    lualine_v = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = {},
  },
}

-- Inserts a component in lualine_c at left section
local function ins_left(component)
  table.insert(config.sections.lualine_c, component)
end

-- Inserts a component in lualine_x ot right section
local function ins_right(component)
  table.insert(config.sections.lualine_x, component)
end

ins_left {
  function()
    return '▊'
  end,
  color = { fg = colors.blue }, -- Sets highlighting of component
  padding = { left = 0, right = 1 }, -- We don't need space before this
}

ins_left {
  -- mode component
  function()
    -- auto change color according to neovims mode
    local mode_color = {
      n = colors.red,
      i = colors.green,
      v = colors.blue,
      [''] = colors.blue,
      V = colors.blue,
      c = colors.magenta,
      no = colors.red,
      s = colors.orange,
      S = colors.orange,
      [''] = colors.orange,
      ic = colors.yellow,
      R = colors.violet,
      Rv = colors.violet,
      cv = colors.red,
      ce = colors.red,
      r = colors.cyan,
      rm = colors.cyan,
      ['r?'] = colors.cyan,
      ['!'] = colors.red,
      t = colors.red,
    }
    vim.api.nvim_command('hi! LualineMode guifg=' .. mode_color[vim.fn.mode()] .. ' guibg=' .. colors.bg)
    return ''
  end,
  color = 'LualineMode',
  padding = { right = 1 },
}

ins_left {
  -- filesize component
  'filesize',
  cond = conditions.buffer_not_empty,
}

ins_left {
  'filename',
  cond = conditions.buffer_not_empty,
  color = { fg = colors.magenta, gui = 'bold' },
}

ins_left { 'location' }

ins_left { 'progress', color = { fg = colors.fg, gui = 'bold' } }

-- Add components to right sections
ins_right {
  'fileformat',
  fmt = string.upper,
  icons_enabled = true, -- I think icons are cool but Eviline doesn't have them. sigh
  color = { fg = colors.green, gui = 'bold' },
}

ins_right {
  'branch',
  icon = '',
  color = { fg = colors.violet, gui = 'bold' },
}

ins_right {
  'diff',
  -- Is it me or the symbol for modified us really weird
  symbols = { added = ' ', modified = '柳 ', removed = ' ' },
  diff_color = {
    added = { fg = colors.green },
    modified = { fg = colors.orange },
    removed = { fg = colors.red },
  },
  cond = conditions.hide_in_width,
}

ins_right {
  function()
    return '▊'
  end,
  color = { fg = colors.blue },
  padding = { left = 1 },
}

-- Now don't forget to initialize lualine
lualine.setup(config)

EVILINE
