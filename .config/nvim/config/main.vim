" Neovim look
if !has('gui_running')
	set t_Co=256
endif

if (has("termguicolors"))
	set termguicolors
endif


" Color scheme
syntax on
set background=dark
let g:gruvbox_italic=1
let g:gruvbox_contrast_dark='hard'
colo gruvbox
hi Normal guibg=NONE ctermbg=NONE


" Essential config
packadd termdebug
let g:termdebug_wide=1

filet indent on
set noet              " noexpandtab
set ch=1              " cmdheight
set hid               " switch buffers w/out saving
set ic                " ignorecase
set lbr               " linebreak
set ls=2              " laststatus
set mouse=a           " enable mouse
set nosmd             " noshowmode
set noswapfile        " disable swap file
set nu                " number
set pa+=**            " path
set sc                " showcmd
set scs               " smartcase
set si                " smartindent
set so=5              " scrolloff
set sw=2 ts=2 sts=2   " tab options
set wrap              " text wrap
set list lcs=tab:\|\  " indentline for tabs

" au FileType cpp :setlocal sw=2 ts=2 sts=2 noet
au BufNewFile,BufRead *.ejs set filetype=html
au BufRead,BufNewFile *.ejs setfiletype html
au BufNewFile,BufRead *.ts setlocal filetype=typescript
au BufNewFile,BufRead *.tsx setlocal filetype=typescript.tsx
au FileType ejs :setfiletype html


" Ignore these files
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx


" CSS highlight fix
hi link cssBorderProp GruvboxAqua
hi link cssDimensionProp GruvboxAqua
hi link cssMediaProp GruvboxAqua
hi link cssPageProp GruvboxAqua
hi link cssPositioningProp GruvboxAqua
hi link cssUIProp GruvboxAqua


" Highlight extra whitespace
" highlight ExtraWhitespace ctermbg=red guibg=red
" match ExtraWhitespace /\s\+$/
" au BufWinEnter * match ExtraWhitespace /\s\+$/
" au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
" au InsertLeave * match ExtraWhitespace /\s\+$/
" au BufWinLeave * call clearmatches()


" Python
let g:python3_host_prog = '/usr/bin/python3'
let g:python2_host_prog = '/usr/bin/python2'


" Documentation
nnoremap <silent> K :call <SID>show_documentation()<cr>

function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	elseif (coc#rpc#ready())
		call CocActionAsync('doHover')
	else
		execute '!' . &keywordprg . " " . expand('<cword>')
	endif
endfunction


" Integrated terminal
let g:term_buf = 0
let g:term_win = 0
function! TermToggle(height)
	if win_gotoid(g:term_win)
		hide
	else
		botright new
		exec "resize " . a:height
		try
			exec "buffer " . g:term_buf
		catch
			call termopen($SHELL, {"detach": 0})
			let g:term_buf = bufnr("")
			set nonumber
			set norelativenumber
			set signcolumn=no
		endtry
		startinsert!
		let g:term_win = win_getid()
	endif
endfunction

" toggle terminal on/off (neovim)
nnoremap tt :call TermToggle(8)<cr>

" terminal go back to normal mode
tnoremap <Esc> <c-\><c-n>
tnoremap :q! <c-\><c-n>:q!<cr>
