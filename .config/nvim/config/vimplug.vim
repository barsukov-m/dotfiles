" Vimplug
call plug#begin('~/.vim/plugged')
	Plug 'SirVer/ultisnips'                  " snippets
	Plug 'honza/vim-snippets'                " required by ultisnips
	Plug 'Yggdroot/indentLine'               " display verical lines for space indentation
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
	Plug 'junegunn/fzf.vim'                  " color picker (Alt+C - HEX, Alt+R - RGB)
	Plug 'kabbamine/vCoolor.vim'
	Plug 'kyazdani42/nvim-web-devicons'
	Plug 'leafgarland/typescript-vim'        " TypeScript support
	Plug 'nvim-treesitter/nvim-treesitter'   " Extra syntax highlighting
	Plug 'lilydjwg/colorizer'                " hex colorizer
	Plug 'mattn/emmet-vim'                   " coc-emmet dependant
	Plug 'mbbill/undotree'                   " undotree (t+u)
	Plug 'mcchrish/nnn.vim'
	Plug 'mg979/vim-visual-multi'
	Plug 'mhinz/vim-startify'
	Plug 'morhetz/gruvbox'
	Plug 'nvim-lualine/lualine.nvim'
	Plug 'romgrk/barbar.nvim'                " bars
	Plug 'sheerun/vim-polyglot'              " language pack
	Plug 'tpope/vim-commentary'              " comments
	Plug 'tpope/vim-surround'                " () {} []
	Plug 'DreamMaoMao/yazi.nvim'             " Yazi
	Plug 'nvim-telescope/telescope.nvim'     " Yazi deps
	Plug 'nvim-lua/plenary.nvim'             " Yazi deps

	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	let g:coc_global_extensions = [
		\ 'coc-eslint',
		\ 'coc-tslint-plugin',
		\ 'coc-json',
		\ 'coc-html',
		\ 'coc-css',
		\ 'coc-prettier',
		\ 'coc-emmet',
		\ 'coc-clangd',
		\ 'coc-python',
		\ ]
call plug#end()
