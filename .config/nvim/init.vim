" ██╗███╗   ██╗██╗████████╗   ██╗   ██╗██╗███╗   ███╗
" ██║████╗  ██║██║╚══██╔══╝   ██║   ██║██║████╗ ████║
" ██║██╔██╗ ██║██║   ██║      ██║   ██║██║██╔████╔██║
" ██║██║╚██╗██║██║   ██║      ╚██╗ ██╔╝██║██║╚██╔╝██║
" ██║██║ ╚████║██║   ██║   ██╗ ╚████╔╝ ██║██║ ╚═╝ ██║
" ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   ╚═╝  ╚═══╝  ╚═╝╚═╝     ╚═╝

" NOTE: if there's something wrong
"       with the vim config,
"       run :CheckHealth

set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

let conf = [
	\ 'vimplug',
	\ 'main',
	\ 'key-mappings',
	\ 'plugins'
	\ ]

for f in conf
	execute 'so ~/.vim/config/' .. f .. '.vim'
endfor
