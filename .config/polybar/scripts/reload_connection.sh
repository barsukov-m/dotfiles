#!/bin/sh

INTERFACE=$(ls /sys/class/net | grep -o "wl.*")
if [[ $INTERFACE ]]; then
	notify-send 'Reloading connection...';
	nmcli device connect $INTERFACE;
else
	notify-send 'Interface is not connected';
fi
